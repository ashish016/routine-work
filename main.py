import util


class Base:
    def __init__(self, next = None, function_attached = None):
        self.next = next
        self.function_attached = function_attached

    def show_data(self):
        print('next is --> ', self.next)
        print('function_attached is --> ', self.function_attached)


class Entity(Base):
    def __init__(self, next, function_attached):
        super().__init__(next, function_attached)


class Action(Base):
    def __init__(self, next, function_attached):
        super().__init__(next, function_attached)


class Branch(Base):
    def __init__(self, next, function_attached, branch):
        super().__init__(next, function_attached)
        self.branch = branch
    # def get_details(self):
    #     super().show_data()
    #     print('branch =',self.branch)


class LinkedList:
    def __init__(self) -> object:
        self.head = None

    def show_data(self):
        if self.head is None:
            print('LinkList is Empty')
            return
        itr = self.head
        strin = ''
        address = ''
        while itr:
            strin += str(itr.function_attached) + ' --> '
            address += str(itr.next) + ' --> '

            itr = itr.next
        print(strin)
        print(address)

    def insert_at_end(self, data):
        if self.head is None:
            node = data
            self.head = node
            return
        itr = self.head
        while itr.next:
            itr = itr.next
        itr.next = data


if __name__ == '__main__':
    ll = LinkedList()
    ll.show_data()
    var = Entity(None, 'show_my_name')
    var.show_data()
    getattr(util, var.function_attached)()
    act = Action(None, 'sum_of_2_nos')
    act.show_data()
    getattr(util, act.function_attached)(5, 10)
    br = Branch(None, 'even_or_odd', [0x151, 0x161])
    print('branch =', br.branch)
    br.show_data()
    getattr(util, br.function_attached)(7)
    ll.insert_at_end(var)
    ll.insert_at_end(act)
    ll.insert_at_end(br)
    ll.show_data()
